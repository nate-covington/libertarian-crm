# Manual Installation

## CiviCRM
https://docs.civicrm.org/installation/en/latest/wordpress/


## Custom Field Setup 

**Option 1: **

Download the [sandbox_crm.sql](data/sandbox_crm.sql) file and import it on top of your your CiviCRM database (this will overwrite all your data so it's only a good idea for brand new installations)

It's also possible to manually [select specific tables](https://forum.civicrm.org/index.php%3Ftopic=925.0.html) if you want to pull these custom fields into an existing CiviCRM installation.

**Option 2:**

- Install the [Configuration Loader](https://civicrm.org/extensions/civicrm-configuration-loader-extension-version-2) extension onto your CiviCRM site (This will need to be installed via FTP as well).
- Once this is installed, you can import the custom fields into your CiviCRM dashboard using this: 
[lp-custom-fields-v3.zip](data/lp-custom-fields-v3.zip)

**Option 3:**

Create your custom fields manually to match below.

![Custom Fields](https://lppamonroe.com/wp-content/uploads/2023/11/custom-fields-manual-option.png)
