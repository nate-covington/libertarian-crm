# Part B: Actually Using CiviCRM 

## Vidas Data Prep Notes:	

Credit: Autumn Pangia

- You may need to save your .csv file as .xls to apply the formulas and manipulations mentioned below, note that you will need to convert back to .csv for upload to the CRM.								
- The CRM template created for Monroe County will not contain data values for your local voters, below lists some custom data fields you will need to update with your own region's details.								

**Custom Data Fields > Voter Registration**

- Most of these fields will require updating of the multiple choice options that apply to your region.(for example, instead of importing all 500+ party affiliations we only created options for those appearing in our voter data)							

- In these custom data fields you may want to leverage more of the fields than we did in rural Monroe County for identifying the granularity of municipalities, you can change the field settings to change what is  now single-line input to a drop-down menu.							
	
Bellow suggestions to change the heading of the data is in the interest of improving the auto-select of the match fields step when importing the data

```
A	vonum	Voter ID					
F	m_name	Middle Name					
H	party_code	Registered Party Affiliation					
O	apartment_number	Supplemental Address 1					
P	address2	Supplemental Address 2					
W	school_district	School					
X	magisterial	Judicial					
Z	state_rep	State Lower House					
AA	state_sen	State Upper House					
AB	us_rep	Congressional District					
BV	dob	Birthdate	
```											

**First Name, Last Name, Middle Name**								

Check for names with apostrophes which the Vidas data converts to symbols							
The previously encountered symbol to search for is \ - consider sorting Last names and then going to the O's as the O followed by symbols should appear at either the beginning or end of the O names.						

There are some first names that have apostrophes, so be sure to check those too.						

Apply formulas to convert names from all caps to mixed caps.							
this is important because if we import all caps then people will receive communications like "Greetings, AUTUMN . . ." which is just asthetically unpleasing.						

Check for hyphenated last names & make sure the 2nd last name is also capitalized (simply search the field for "-"; it's probably worth checking first names also)						

Below is a summary of the steps and formulas to use to convert all-caps data to mixed-cap format:							

Insert 3 columns to the right of the column of data (last name, first name, middle name) and apply these formulas to create the properly formatted names						
`Source Data	 =LOWER(D24)	 =LEFT(D24,1)	 =REPLACE(E24,1,1,F24)		`
`EXAMPLE	example	E	Example		`

Copy the resulting column of data in the Replace formula column, paste value over the source data.						
When you're done manipulating the data to populate the properly formatted content, delete the columns added with the formulas to tidy up your spreadsheet.						
								
**Suffix**							
Sort and update the format of Suffixes to align to the CRM drop-down menu options:							
	Jr.						
	Sr.						
	II						
	III						
	IV						
	V						
	VI						
	VII						

Suffixes not on the drop-down option won't populate, if you think it's a valid suffix that should be added then the field's drop-down details would need to be updated in CRM							

**Address Data:	**							

Concatenate Address so data is importable							
Use the concatenate formula to combine House # and Street Name. (Use "house_number_display" in column M as this is where addresses like "532 1/2" will appear correctly)						

Label this new column of data "Address"						
Make sure you've copied/pasted values to eliminate the formula when you are done						
Give special attention to House numbers with franctions . . . Below is an example of the formula you will need to use for those:							
`Source House #	Source Street	 =CONCATENATE((TEXT(D55,"# ??/??"))," ",E55)			`
> 532 1/2	BARRY ST	532  1/2  BARRY ST	

State data must be spelled out for proper upload to the CRM, Replace all "PA" in the State column with "Pennsylvania"							
If you don't do this then the will not upload properly.						
																
**School District**	
Voter data for Monroe county specified by sub-districts and we have chosen to simplify by just identifying school districts at the highest level.  You may choose to do this differently, you will need to update the drop-down menu in any event (it is found in the custom fields under Voter Registration).							
								
Sort the data in the "school_district" field and change the codes in voter data to align to the basic school district options in your county, unless you opt to update the custom field to include all the ways the school districts are identified in your area, be certain that the Label (which is all users will see when updating info in CRM) clearly describes the differences if the parent district name is duplicated (as it is in Monroe County with Pocono Mt. East and Pocono Mt. West).							
	East Stroudsburg					
	Pleasant Valley					
	Pocono Mountain					
	Stroudsburg					
								
**Other Data:**						
Add data that isn't in the Vidas data but is necessary upstream selections for downstream Vidas data to populate into CRM contact details (I.E. the county data won't populate if Country and State aren't identified)							

**Country:**
All must be listed as United States						


## Import your Data
CiviCRM -> Contacts -> Import

**WHEN IMPORTING:**

**In the Choose Data Source Step:**
- Choose Date Format "**mm/dd/yy OR mm-dd-yy**"
- Even though the full data in the formula field appears to show 4-digit years, the cell view is only 2-digits and that is what's recognized when importing the CRM

**In the Match Field Step:**
- Consider clicking on the option at the bottom to "save this field mapping."
- In the next screen you may receive data import failures and you'll save some time on your next attempt if you can just click on "Use Saved Field Mapping" your next time through. 
- Saved field mapping details are basically stored with the logic of "column A = Last Name" "column B = First Name" etc; so if your columns are in a different order the field mapping isn't smart in the sense that it aligns the selected mapping to the file's header row.  Keep this in mind and if you add columns to your file for the next upload you should add them to the far right, then at least the field mapping will work for most of your data elements.

**In the final "Preview" Step:**

- Review the top of the screen - if you have Rows With Errors you should click on the "Download Errors" button and review the excel sheet which will tell you exactly which fields contained errors for each data record; use this to figure out what isn't formatted correctly and make the corrections in your file and attempt to re-upload. 	
- Common error is having data in a field that is programmed as a drop-down menu, but the data you identified isn't one of the items available for selection.  These fields aren't case-sensitive (I.E. uploading township of "POLK" for a dropdown menu with "Polk" will work).  Review the CRM setup to ensure that spelling is correct and that all necessary menu items have been listed.

Reference File:
[Vidas to CRM Data Prep Notes.xlsx](docs/CRM-Data-Prep-Notes.xlsx)


## SMS

We had this working pretty easily with the [Twilio SMS](https://docs.civicrm.org/user/en/latest/sms-text-messaging/set-up/#configuring-a-twilio-sms-gateway) platform.

This also [requires two "CiviRules" to be created](https://civicrm.stackexchange.com/questions/38041/how-to-convert-a-stop-message-sent-to-an-sms-provider-into-the-do-not-sms-priv) for marking when people opt in and out for SMS.


## Email
Email needs to be configured within Wordpress AND within CiviCRM.  For Wordpress I typically suggest the [WP Mail SMTP](https://wordpress.org/plugins/wp-mail-smtp/) plugin.  

The SMTP configuration inside CiviCRM is in two places:

 - Administer -> Communications -> FROM Email Addresses
 - Administer -> System Settings -> Outbound Email
