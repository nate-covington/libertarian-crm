
# Part A: Installation

## Option 1: Pre-Packaged Installer
Currently includes Wordpress 6.4., CiviCRM 5.67.1, [Akeeba Backup 8.1.1](https://www.akeeba.com/products/akeeba-backup-wordpress.html)

## Installer Package:
(right-click, save-as, it should be around 180mb)
https://lppamonroe.com/wp-content/uploads/libertarian-crm-akeeba-installer-package-113023.jpa

## Instructions:

1. Upload the Akeeba backup file (.jpa) along with the [Kickstart](https://www.akeeba.com/products/akeeba-kickstart.html) installer to your web server (via FTP)
2. Visit the kickstart.php file your web browser and use the wizard to run the installer.  (example.com/kickstart.php)
3. This will install the Wordpress and CiviCRM software and load the data into the databases, the only part it doesn't handle automatically is the `civicrm.settings.php` file, you'll need to update this file via FTP as well:
`/public_html/wp-content/uploads/civicrm/civicrm.settings.php`
4. In this file and add your file system, database connection information, and new [site key](https://civicrm.stackexchange.com/questions/548/changing-civicrm-site-key-in-the-civicrm-settings-php-file), [secret tokens](https://docs.civicrm.org/sysadmin/en/latest/setup/secret-keys/#generate-secrets) onto lines: 73, 77-79, 117, 187-190, 267, 333, 354, 381
