import pandas as pd

def process_voter_data():
    # Step 1: Read voter registration data and Twilio phone numbers data
    voter_file = 'final-civicrm.csv'
    twilio_file = 'final-twilio.csv'
    
    # Explicitly specify dtype to avoid FutureWarning
    voter_df = pd.read_csv(voter_file, dtype={'first_name': 'str', 'last_name': 'str'})
    twilio_df = pd.read_csv(twilio_file)

    # Convert 'To' column to string and remove "1" before each phone number
    twilio_df['To'] = twilio_df['To'].astype(str).str.replace('^1', '', regex=True)

    # Step 2: Print delivered and undelivered phone numbers
    delivered_numbers = twilio_df.loc[(twilio_df['Status'] == 'delivered') | (twilio_df['Status'] == 'sent'), 'To'].drop_duplicates()
    undelivered_numbers = twilio_df.loc[twilio_df['Status'] == 'undelivered', 'To'].drop_duplicates()

    print("Delivered Phone Numbers:")
    print(delivered_numbers.tolist())

    print("\nUndelivered Phone Numbers:")
    print(undelivered_numbers.tolist())

    # Save delivered phone numbers to 'twilio-mobiles.csv' with corresponding name and address
    save_phone_numbers(delivered_numbers, 'twilio-mobiles.csv', voter_df)

    # Save undelivered phone numbers to 'twilio-landlines.csv' with corresponding name and address
    save_phone_numbers(undelivered_numbers, 'twilio-landlines.csv', voter_df)

def save_phone_numbers(phone_numbers, output_file, voter_df):
    # Drop duplicates from the phone_numbers list
    phone_numbers = phone_numbers.drop_duplicates()
    
    phone_numbers_df = pd.DataFrame({'Phone Numbers': phone_numbers.tolist()})
    # Convert the data type to string for merging
    phone_numbers_df['Phone Numbers'] = phone_numbers_df['Phone Numbers'].astype(str)
    
    # Convert 'home_phone' column in voter_df to string for merging
    voter_df['home_phone'] = voter_df['home_phone'].astype(str)
    
    # Merge with 'final-civicrm.csv' to get corresponding name and address
    merged_df = pd.merge(phone_numbers_df, voter_df[['home_phone', 'first_name', 'last_name']], left_on='Phone Numbers', right_on='home_phone', how='left')
    
    # Specify the columns to include in the final CSV file
    columns_to_include = ['Phone Numbers', 'first_name', 'last_name']
    
    # Sort the DataFrame by 'last_name'
    merged_df = merged_df.sort_values(by=['last_name'], ignore_index=True)
    
    # Save the selected columns to a new CSV file
    merged_df[columns_to_include].to_csv(output_file, index=False)
    print(f"\nProcessed data saved to {output_file}")

    # Iterate through the CSV file and look up first_name and last_name for each row
    df_to_update = pd.read_csv(output_file)
    for index, row in df_to_update.iterrows():
        phone_number = str(row['Phone Numbers'])
        matching_row = voter_df[voter_df['home_phone'] == phone_number]
        if not matching_row.empty:
            # Explicitly convert to string to avoid FutureWarning
            df_to_update.at[index, 'first_name'] = str(matching_row['first_name'].values[0])
            df_to_update.at[index, 'last_name'] = str(matching_row['last_name'].values[0])

    # Sort the DataFrame by 'last_name'
    df_to_update = df_to_update.sort_values(by=['last_name'], ignore_index=True)

    # Save the updated data back to the CSV file
    df_to_update.to_csv(output_file, index=False)
    print(f"\nUpdated data saved to {output_file}")

# Example usage
process_voter_data()
