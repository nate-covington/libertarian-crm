# CiviCRM for Libertarians (Unofficial)

With SMS Integration:
![Screenshot](https://lppamonroe.com/wp-content/uploads/2023/11/civicrm-test-twilio-scaled.jpg)
![Screenshot](https://lppamonroe.com/wp-content/uploads/2023/11/civicrm-test-twilio-reply-scaled.jpg)

- Prepared by [Nate Covington](https://www.natecovington.com/) of [Covington Creations, LLC](https://www.covingtoncreations.com/), a professional web design & development studio located in the Poconos.  
- [Monroe County Libertarians](https://lppamonroe.com/) (LPPA Affiliate Demo Site)

## Prerequisites:

You will need your own domain, hosting plan, and installation of [Wordpress](https://www.natecovington.com/politics/lppa-wordpress-child-theme-kit-candidate-and-affiliate) running before proceeding.  The following guide will generally work for any flavor of CiviCRM, including Drupal and Joomla.  

You will need to install CiviCRM into your Wordpress (or Drupal/Joomla) using FTP because it's most likely too large to install on a standard web server.  

You have the option of configuring one database for both Wordpress and CiviCRM, or two separate databases.  Database users will need to be added and configured prior to actual installation.  


## Installing

Option 1: [Pre Packaged Installer](docs/packaged-installer.md)

Option 2: [Full Manual Install](docs/manual-install.md)


## Cron

This is the "heartbeat" script that runs CiviCRM: sending scheduled messages, etc.  It needs to be [configured on each server](https://docs.civicrm.org/sysadmin/en/latest/setup/jobs/) so it's typically a tech support issue as various hosting companies require different cron configurations.  


## Getting Started with CiviCRM

Now that you've got CiviCRM and the Voter Information custom fields created, you're ready to actually dive in!  

Usage Guide:
[Importing Data, SMS Configuration, Email Configuration (SMTP)](docs/usage.md)


## Merging Twilio "Delivered, Undelivered, Sent" back to Mobile & Landline Fields:
Custom [Python script](python/merge.py) that takes your Twilio data after sending to an initial batch of contacts, it splits the resulting data into Mobile and Landline fields so that subsequent SMS sends only go to the mobile numbers.    


## Support

[Libertarian CiviCRM \[Matrix\] Room](https://matrix.to/#/#civicrm:lppamonroe.com)
